import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试
  const { getByLabelText, getByTestId } = render(<OrderComponent/>);
  // setup组件
  const button = getByLabelText('submit-button');
  const input = getByLabelText('number-input');
  const status = getByTestId('status');

  // Mock数据请求
  const data = { status : "success"};
  axios.get.mockResolvedValue({data});
  // 触发事件
  await fireEvent.click(button);
  fireEvent.change(input, {target: { value: '1234' }});
  // 给出断言
  expect(status).toHaveTextContent('success');
  // --end->
});
