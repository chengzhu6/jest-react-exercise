import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import TitleWithProps from '../TitleWithProps';

// <--start
// TODO: 少什么就引点儿什么吧。
// --end->

test('TitleWithProps组件渲染内容', () => {
  // <--start
  // TODO 1: 给出正确的assertion，测试Title组件渲染内容
  // eslint-disable-next-line react/jsx-no-undef
  const actual = <TitleWithProps name="job" />;
  const renderResult = render(actual);

  expect(renderResult.queryByText('Hello job')).not.toBeNull();

  // --end->
});
